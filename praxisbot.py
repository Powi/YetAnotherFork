"""

Copyright (C) 2018 MonaIzquierda (mona.izquierda@gmail.com)
Copyright (C) 2020-2021 Powi (powi@powi.fr)

This file is part of PraxisBot.

PraxisBot is free software: you can redistribute it and/or  modify
it under the terms of the GNU Affero General Public License, version 3,
as published by the Free Software Foundation.

PraxisBot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with PraxisBot.  If not, see <http://www.gnu.org/licenses/>.

"""

import sys
import io
import traceback
import shlex
import copy
import inspect
import re
import random
import sqlite3
import emojis
import nextcord
import datetime
from pytz import timezone
from functools import wraps

class RedirectOutput():
	def __init__(self, destout, desterr):
		self.destout = destout
		self.desterr = desterr
		self.old_stdout = sys.stdout
		self.old_stderr = sys.stderr

	def __enter__(self):
		sys.stdout = self.destout
		sys.stderr = self.desterr

	def __exit__(self, *args):
		sys.stdout = self.old_stdout
		sys.stderr = self.old_stderr

################################################################################
# Exceptions
################################################################################

class Error(Exception):
    pass

class TooLongScriptError(Error):
	pass

class AdminPermissionError(Error):
	pass

class BotOwnerPermissionError(Error):
	pass

class ScriptPermissionError(Error):
	pass

class ParameterPermissionError(Error):
	def __init__(self, parameter):
		self.parameter = parameter

class CommandNotFoundError(Error):
	def __init__(self, command):
		self.command = command

class ObjectNameError(Error):
	def __init__(self, parameter, name):
		self.parameter = parameter
		self.name = name

class ObjectIdError(Error):
	def __init__(self, parameter, name):
		self.parameter = parameter
		self.name = name

class IntegerError(Error):
	def __init__(self, parameter, name):
		self.parameter = parameter
		self.name = name

class RegexError(Error):
	def __init__(self, regex):
		self.regex = regex

################################################################################
# Decorators
################################################################################

def command(func):
	@wraps(func)
	def wrapper(self, scope, command, options, lines, **kwargs):
		d = inspect.getdoc(func)
		return func(self, scope, command, options, lines, description=d, **kwargs)

	return wrapper

def permission_admin(func):
	@wraps(func)
	def wrapper(self, scope, command, options, lines, **kwargs):
		if scope.permission < UserPermission.Admin:
			raise AdminPermissionError()
		return func(self, scope, command, options, lines, **kwargs)
	return wrapper

def permission_script(func):
	@wraps(func)
	def wrapper(self, scope, command, options, lines, **kwargs):
		if scope.permission < UserPermission.Script:
			raise ScriptPermissionError()
		return func(self, scope, command, options, lines, **kwargs)
	return wrapper

def permission_botowner(func):
	@wraps(func)
	def wrapper(self, scope, command, options, lines, **kwargs):
		if scope.permission < UserPermission.BotOwner:
			raise BotOwnerPermissionError()
		return func(self, scope, command, options, lines, **kwargs)
	return wrapper

################################################################################
# Scope
################################################################################

class ExecutionBlockIf:
	def __init__(self, e):
		self.execute = e
		self.terminated = False

	async def execute_script(self, scope, command, commandline):
		if command == "endif":
			self.terminated = True
			return

		if command == "else":
			self.execute = not self.execute
			return

		if self.execute:
			await scope.shell.execute_command(scope, commandline)

class ExecutionBlockFor:
	def __init__(self, var, list):
		self.var = var
		self.list = list
		self.cmds = []
		self.terminated = False

	async def execute_script(self, scope, command, commandline):
		if command == "endfor":
			self.terminated = True
			for i in self.list:
				scope.vars[self.var] = i
				for c in self.cmds:
					await scope.shell.execute_command(scope, c)
					if scope.abort:
						return

		elif len(self.list):
			self.cmds.append(commandline)

class ExecutionScope:
	def __init__(self, shell, guild, prefixes):
		self.shell = shell
		self.prefixes = prefixes
		self.guild = guild
		self.channel = None
		self.user = None
		self.message = None

		self.permission = UserPermission.Member

		self.iter = 0
		self.vars = {}
		self.member_vars = {}
		self.session_vars = {}
		self.plugin_vars = {}
		self.blocks = []
		self.abort = False
		self.deletecmd = False
		self.verbose = 2

	async def execute_script(self, command, commandline):
		"""
		Take care of conditions (if, for, ...). Return True if the command must not be executed
		"""
		if not len(self.blocks):
			await self.shell.execute_command(self, commandline)
			return

		b = self.blocks[len(self.blocks)-1]
		await b.execute_script(self, command, commandline)
		if b.terminated:
			self.blocks.pop()

	def create_subscope(self):
		subScope = ExecutionScope(self.shell, self.guild, self.prefixes)

		subScope.channel = self.channel
		subScope.user = self.user
		subScope.message = self.message
		subScope.permission = self.permission

		subScope.iter = self.iter
		subScope.vars = self.vars
		subScope.member_vars = self.member_vars
		subScope.session_vars = self.session_vars
		subScope.blocks = self.blocks
		subScope.abort = self.abort
		subScope.deletecmd = self.deletecmd
		
		return subScope

	def continue_from_subscope(self, subScope):
		self.iter = subScope.iter
		self.vars = subScope.vars
		self.member_vars = subScope.member_vars
		self.session_vars = subScope.session_vars
		self.abort = subScope.abort
		self.deletecmd = subScope.deletecmd

	def format_text(self, text):
		if not text:
			return ""

		pattern = re.compile("\{\{([^\}]+)\}\}")

		formatedText = ""
		textIter = 0
		matchIters = pattern.finditer(text)
		for match in matchIters:
			formatedText = formatedText + text[textIter:match.start()]
			textIter = match.end()

			#Process tag
			tag = match.group(1).strip()
			tagOutput = match.group()
			
			area = self.vars
			
			if tag.find('|') >= 0:
				tag = random.choice(tag.split("|"))
				tagOutput = tag

			tuser = self.user
			user_chk = re.fullmatch('([*@#]?user(?:.*)?)=(.*)', tag)
			if user_chk:
				subUser = user_chk.group(2).strip()
				if subUser in self.vars:
					subUser = self.vars[subUser].strip()
				tuser = self.shell.find_member(subUser, self.guild)
				tag = user_chk.group(1)
			elif tag.find(':') >= 0:
				mchk = tag.split(':')[0]
				if mchk.lower() == "user":
					tuser = self.user
				else:
					tuser = self.shell.find_member(mchk, self.guild)
				if tuser and tuser.id:
					print("member_vars : {}".format(self.member_vars))
					if not tuser.id in self.member_vars.keys():
						self.member_vars[tuser.id] = {}
					area.update(self.member_vars[tuser.id])
					tag = tag.split(':')[1]
					print("Tag = `{}`".format(tag))

			tmessage = self.message
			message_chk = re.fullmatch('([*]?message)=(.*)', tag)
			if message_chk:
				subMsg = message_chk.group(2).strip()
				if subMsg in self.vars:
					subMsg = self.vars[subMsg]
				tmessage = self.shell.find_message(subMsg, self.channel)
				tag = message_chk.group(1)

			tchannel = self.channel
			channel_chk = re.fullmatch('([*#]?channel)=(.*)', tag)
			if channel_chk:
				subChan = channel_chk.group(2).strip()
				if subChan in self.vars:
					subChan = self.vars[subChan]
				tchannel = self.shell.find_channel(subChan, self.guild)
				tag = channel_chk.group(1)

			temoji = None
			emoji_chk = re.fullmatch('([*@]?emoji)=(.*)', tag)
			if emoji_chk:
				subEmoji = emoji_chk.group(2).strip()
				if subEmoji in self.vars:
					subEmoji = self.vars[subEmoji]
				temoji = self.shell.find_emoji(subEmoji, self.guild)
				tag = emoji_chk.group(1)

			trole = None
			role_chk = re.fullmatch('([*@]?role)=(.*)', tag)
			if role_chk:
				subRole = role_chk.group(2).strip()
				if subRole in self.vars:
					subRole = self.vars[subRole]
				trole = self.shell.find_role(subRole, self.guild)
				tag = role_chk.group(1)

			if tag.lower() == "server" and self.guild:
				tagOutput = self.guild.name
			elif tag.lower() == "*server" and self.guild:
				tagOutput = self.guild.id
			elif tag.lower() == "n":
				tagOutput = "\n"
			elif tag.lower() == "p":
				tagOutput = "\n\n"
			elif tag.lower() == "t":
				tagOutput = "\t"
			elif tag.lower() == "now":
				date = datetime.datetime.now(timezone('Europe/Paris'))
				tagOutput = date.strftime("%Y-%m-%d %H:%M:%S")
			elif tag.lower() == "channel" and tchannel:
				tagOutput = tchannel.name
			elif tag.lower() == "#channel" and tchannel:
				tagOutput = tchannel.mention
			elif tag.lower() == "*channel" and tchannel:
				tagOutput = tchannel.id
			elif tag.lower() == "message" and tmessage:
				tagOutput = tmessage.jump_url
			elif tag.lower() == "*message" and tmessage:
				tagOutput = tmessage.id
			elif tag.lower() == "emoji" and temoji:
				if type(temoji) == str:
					tagOutput = temoji
				else:
					tagOutput = temoji.name
			elif tag.lower() == "@emoji" and temoji:
				tagOutput = str(temoji)
			elif tag.lower() == "*emoji" and temoji:
				if type(temoji) == str:
					tagOutput = temoji
				else:
					tagOutput = temoji.id
			elif tag.lower() == "role" and trole:
				tagOutput = trole.name
			elif tag.lower() == "@role" and trole:
				tagOutput = trole.mention
			elif tag.lower() == "*role" and trole:
				tagOutput = trole.id
			elif tag.lower() == "#user" and tuser:
				tagOutput = "{}#{}".format(tuser.name,tuser.discriminator)
			elif tag.lower() == "@user" and tuser:
				tagOutput = tuser.mention
			elif tag.lower() == "*user" and tuser:
				tagOutput = tuser.id
			elif tag.lower() == "user" and tuser:
				tagOutput = tuser.display_name
			elif tag.lower() == "user_time" and tuser:
				tagOutput = str(tuser.created_at)
			elif tag.lower() == "#user_avatar" and tuser:
				tagOutput = tuser.default_avatar.replace(static_format="png").url
				if tuser.avatar:
					tagOutput = tuser.avatar.replace(static_format="png").url
			elif tag.lower() == "user_avatar" and tuser:
				tagOutput = tuser.display_avatar.replace(static_format="png").url
			elif tag[0] == "*" and tag[1:]:
				var = tag[1:] in area and area[tag[1:]] or tag[1:]
				if var.strip() == 0:
					tagOutput = 0
				elif var.find("\n") >= 0:
					set_var = var.split("\n")
					tagOutput = str(len(set_var))
				else:
					tagOutput = str(len(var))
			elif tag[0] == "," and tag[1:] in area:
				set_var = area[tag[1:]].split("\n")
				tagOutput = ", ".join(set_var)
			elif tag[0] == "_" and tag[1:]:
				nb = tag[1:] in area and area[tag[1:]] or tag[1:]
				try:
					nb = int(nb)
				except ValueError:
					nb = len(nb)
				tagOutput = " "*nb
			elif tag[0] == "#" and tag[1:] and re.fullmatch('#(.*)#([0-9]{1-3})', tag):
				print("# RECOGNIZED")
				var_table = re.fullmatch('#(.*)#(.*)', tag)
				if var_table:
					var_name = var_table.group(1)
					item_id = var_table.group(2) in area and area[var_table.group(2)] or var_table.group(2)
					print("VAR: {}, ITEM: {}".format(var_name,item_id))
					if item_id.isnumeric() and var_name in area:
						var_table = area[var_name].split("\n")
						item_id = int(item_id)
						print("VAR_TABLE: {}, ITEM: {}".format(var_table,item_id))
						if len(var_table) > item_id:
							tagOutput = var_table[item_id]
			elif tag in area:
				tagOutput = area[tag]
			else:
				tagOutput = tag
			formatedText = formatedText + str(tagOutput)

		formatedText = formatedText + text[textIter:]

		return formatedText

################################################################################
# Shell
################################################################################

class UserPermission:
	Member=0
	Script=1
	Admin=2
	Owner=3
	BotOwner=9

class Shell:
	def __init__(self, client, dbprefix, dbcon, dbfile):
		self.plugins = []
		self.client = client
		self.dbprefix = dbprefix
		self.dbcon = dbcon
		self.dbfile = dbfile

	async def print_info(self, scope, msg):
		if scope.verbose >= 2:
			try:
				await scope.channel.send(msg)
			except nextcord.errors.HTTPException:
				f=io.StringIO(msg)
				await scope.channel.send("The help message is too long for discord, I send it in txt format.",file=nextcord.File(f,filename="help.txt"))
		return

	async def print_debug(self, scope, msg):
		if scope.verbose >= 3:
			await scope.channel.send(":large_blue_circle: {}".format(msg))
		return

	async def print_success(self, scope, msg):
		if scope.verbose >= 2:
			await scope.channel.send(":white_check_mark: {}".format(msg))
		return

	async def print_permission(self, scope, msg):
		if scope.verbose >= 1:
			await scope.channel.send(":closed_lock_with_key: {}".format(msg))
		return

	async def print_error(self, scope, msg):
		if scope.verbose >= 1:
			await scope.channel.send(":no_entry: {}".format(msg))
		return

	async def print_fatal(self, scope, msg):
		if scope.verbose >= 1:
			await scope.channel.send(":skull_crossbones: {}".format(msg))
		return
	
	def is_plugin_loaded(self, plugin):
		for p in self.plugins:
			if type(p) == plugin:
				return True
		return False

	def load_plugin(self, plugin):
		"""
		Create an instance of a plugin and register it
		"""
		if self.is_plugin_loaded(plugin):
			print("Plugin {} already loaded".format(plugin.name))
			return
		try:
			instance = plugin(self)
			self.plugins.append(instance)
			print("Plugin {0} loaded".format(plugin.name))
		except:
			print(traceback.format_exc())
			print("Plugin {0} can't be loaded".format(plugin.name))

	def find_command_and_options(self, commandline, prefixes):
		for prefix in prefixes:
			if commandline.find(prefix) == 0:
				commandline = commandline[len(prefix):]
				lines = commandline.split("\n")
				command = lines[0].split(" ")[0:1][0].strip()
				options = lines[0][len(command):].strip()
				if len(command.strip()) == 0:
					return None
				return (command, options, lines[1:])
		return None

	def create_scope(self, server, prefixes):
		scope = ExecutionScope(self, server, prefixes)

		with self.dbcon:
			c = self.dbcon.cursor()
			vars = scope.shell.get_sql_data("variables",["name","value"],{"discord_sid":scope.guild.id},True) or []
			for row in vars:
				scope.vars[row[0]] = row[1]
			mvars = scope.shell.get_sql_data("member_variables",["name","discord_mid","value"],{"discord_sid":scope.guild.id},True) or []
			for row in mvars:
				if row[1] not in scope.member_vars.keys():
					scope.member_vars[row[1]] = {}
				scope.member_vars[row[1]][row[0]] = row[2]

		return scope

	async def execute_command(self, scope, commandline):
		try:
			if scope.iter > 128:
				raise TooLongExecutionError()

			parsedCommand = self.find_command_and_options(commandline, scope.prefixes)
			if not parsedCommand:
				return False

			for p in self.plugins:
				if await p.execute_command(scope, parsedCommand[0], parsedCommand[1], parsedCommand[2]):
					scope.iter = scope.iter+1
					return True

			raise CommandNotFoundError(parsedCommand[0])

		except CommandNotFoundError as e:
			await self.print_error(scope, "Command `{}` not found.".format(e.command))
			scope.abort = True
		except BotOwnerPermissionError as e:
			await self.print_permission(scope, "This command is restricted to Bot Owner.")
			scope.abort = True
		except AdminPermissionError as e:
			await self.print_permission(scope, "This command is restricted to administrators.")
			scope.abort = True
		except ScriptPermissionError as e:
			await self.print_permission(scope, "This command is restricted to scripts and administrators.")
			scope.abort = True
		except ParameterPermissionError as e:
			await self.print_permission(scope, "You are not allowed to use the parameter `{}`.".format(e.parameter))
			scope.abort = True
		except ObjectNameError as e:
			await self.print_error(scope, "{} must be a letter followed by alphanumeric characters.".format(e.parameter))
			scope.abort = True
		except ObjectIdError as e:
			await self.print_error(scope, "{} must be a number.".format(e.parameter))
			scope.abort = True
		except IntegerError as e:
			await self.print_error(scope, "{} must be a number.".format(e.parameter))
			scope.abort = True
		except RegexError as e:
			await self.print_error(scope, "`{}` is not a valid regular expression.".format(e.regex))
			scope.abort = True
		except sqlite3.OperationalError as e:
			print(traceback.format_exc())
			await self.print_fatal(scope, "**SQL error.** Please contact <@203135242813440001>.\nCommand line: `{}`".format(commandline))
			scope.abort = True
		except Exception as e:
			print(traceback.format_exc())
			await self.print_fatal(scope, "**PraxisBot Internal Error.** Please contact <@203135242813440001>.\nException: ``{}``\nCommand line: `{}`".format(type(e).__name__,commandline))
			scope.abort = True

		return False

	async def execute_script(self, scope, script):
		lines = script.split("\n");

		for l in lines:
			l = l.strip()

			parsedCommand = self.find_command_and_options(l, scope.prefixes)
			if not parsedCommand:
				continue

			await scope.execute_script(parsedCommand[0], l)

			if scope.abort:
				break

	async def send(self, channel, text, e=None):
		if e:
			return await self.client.send(channel, text, embed=e)
		else:
			return await self.client.send(channel, text)

	def find_server(self, server_name):
		for s in self.client.guilds:
			if s.id == server_name:
				return s
		return None

	def find_channel(self, chan_name, guild):
		if not chan_name:
			return None
		print("searching for chan {}".format(chan_name))
		chan_name = str(chan_name).strip()
		for c in guild.channels:
			if c.name == chan_name:
				return c
			elif str(c.id) == chan_name:
				return c
			elif "<#{}>".format(c.id) == chan_name:
				return c
			elif "#{}".format(c.id) == chan_name:
				return c
		print("No chan found with name {}".format(chan_name))
		return None

	def find_member(self, member_name, guild):
		print("Trying to find {} from {}".format(member_name,guild.name))
		if not member_name:
			return None

		member_name = member_name.strip()
		print("Il y a {} membre dessus".format(len(guild.members)))
		for m in guild.members:
			print("\tTrying with {}".format(m.id))
			if "<@{}>".format(m.id) == member_name:
				return m
			if "<@!{}>".format(m.id) == member_name:
				return m
			elif "{}#{}".format(m.name,m.discriminator) == member_name:
				return m
			elif str(m.id) == member_name:
				return m
		print("No member found")
		return None

	def find_role(self, role_name, guild):
		if not role_name:
			return None

		for r in guild.roles:
			if "<@&{}>".format(r.id) == role_name:
				return r
			elif str(r.id) == role_name:
				return r
			elif r.name == role_name:
				return r
		print("No role found")
		return None

	def find_emoji(self, emoji_name, guild):
		if not emoji_name:
			return None

		for e in guild.emojis:
			if "<:{}:{}>".format(e.name,e.id) == emoji_name:
				return e
			elif "<a:{}:{}>".format(e.name,e.id) == emoji_name:
				return e
			elif e.id == emoji_name:
				return e
			elif e.name == emoji_name:
				return e
		if emojis.get(emoji_name):
			return list(emojis.get(emoji_name))[0]
			
		print("No emoji found")
		return None

	def find_message(self, message, channel):
		if not message:
			return None
		
		if type(message) == int or (type(message) == str and message.isnumeric()):
			return channel.get_partial_message(int(message))
		
		elif type(message) == str and message.startswith("http"):
			try:
				link = message.split('/')
				guild = self.find_server(int(link[-3]))
				chan = self.find_channel(int(link[-2]),guild)
				return chan.get_partial_message(int(link[-1]))
			except (ValueError, AttributeError):
				print ("ValueError, returning None")
				return None
			return None

	def get_default_channel(self, guild):
		for c in guild.channels:
			if c.type == nextcord.ChannelType.text:
				return c
		return None

	async def add_roles(self, member, roles):
		try:
			await self.client.add_roles(member, *roles)
		except:
			pass
			return False
		return True

	async def remove_roles(self, member, roles):
		try:
			await self.client.remove_roles(member, *roles)
		except:
			pass
			return False
		return True

	async def change_roles(self, member, rolesToAdd, rolesToRemove):

		roles = member.roles
		rolesAdded = []
		rolesRemoved = []
		for r in rolesToRemove:
			if r in roles:
				roles.remove(r)
				rolesRemoved.append(r)
		for r in rolesToAdd:
			if not r in roles:
				roles.append(r)
				rolesAdded.append(r)

		await member.edit(roles=roles)
		return (rolesAdded, rolesRemoved)

	def dbtable(self, name):
		return self.dbprefix+name

	def create_sql_table(self, tablename, fields):
		sqlQuery = "CREATE TABLE IF NOT EXISTS {} ({})".format(self.dbtable(tablename),", ".join(fields))
		self.dbcon.execute(sqlQuery);

		for f in fields:
			try:
				sqlQuery = "ALTER TABLE {} ADD {}".format(self.dbtable(tablename),f)
				self.dbcon.execute(sqlQuery);
			except sqlite3.OperationalError:
				pass

	def get_sql_data(self, tablename, fields, where, array=False):
		sqlQuery = "SELECT {} FROM {}".format(", ".join(fields),self.dbtable(tablename))
		vars = list(where.values())
		if where:
			sqlQuery+=" WHERE {} = ?".format(" = ? AND ".join(where.keys()))

		c = self.dbcon.cursor()
		datas = c.execute(sqlQuery,vars)
		r = c.fetchall() if array else c.fetchone()
		if r:
			return r

		return None

	def set_sql_data(self, tablename, fields, where, id="id"):
		idFound = self.get_sql_data(tablename, [id], where)
		db = self.dbtable(tablename)
		if idFound:
			sqlQuery = "UPDATE {} SET {} = ? WHERE {} = ?".format(db," = ?, ".join(fields.keys()),id)
			vars = list(fields.values()) + list(idFound)
			self.dbcon.execute(sqlQuery, vars)
		else:
			fnw = list(fields)+list(where)
			sqlQuery = "INSERT INTO {} ({}) VALUES ({})".format(db,", ".join(fnw),", ".join(["?"]*len(fnw)))
			vars = list(fields.values()) + list(where.values())
			self.dbcon.execute(sqlQuery, vars)

	def update_sql_data(self, tablename, fields, where):
		db = self.dbtable(tablename)
		sqlQuery = "UPDATE {} SET {} = ?".format(db, " = ?, ".join(fields.keys()))
		vars = list(fields.values()) + list(where.values())
		first = True
		if where:
			sqlQuery += " WHERE {} = ?".format(" = ? AND ".join(where))
		self.dbcon.execute(sqlQuery, vars)

	def add_sql_data(self, tablename, fields):
		sqlQuery = "INSERT INTO {} ({}) VALUES ({})".format(self.dbtable(tablename),", ".join(fields.keys()),", ".join(["?"]*len(fields)))
		vars = list(fields.values())
		c = self.dbcon.cursor()
		c.execute(sqlQuery, vars)
		return c.lastrowid

	def delete_sql_data(self, tablename, where):
		sqlQuery = "DELETE FROM {} WHERE {} = ?".format(self.dbtable(tablename)," = ? AND ".join(where.keys()))
		vars = list(where.values())
		self.dbcon.execute(sqlQuery, vars)

################################################################################
# Plugin
################################################################################

class Plugin:
	"""
	Base class of all plugins
	"""

	def __init__(self, shell):
		self.shell = shell
		self.cmds = {}

	async def on_loop(self, scope):
		return

	async def list_commands(self, server):
		return list(self.cmds.keys())

	async def execute_unregistered_command(self, scope, command, options, lines):
		return False

	async def execute_command(self, scope, command, options, lines):
		if command in self.cmds:
			scope.iter = scope.iter+1
			await self.cmds[command](scope, command, options, lines)
			return True
		return await self.execute_unregistered_command(scope, command, options, lines)

	async def on_member_join(self, scope):
		return False

	async def on_member_leave(self, scope):
		return False

	async def on_ban(self, scope):
		return False

	async def on_unban(self, scope):
		return False

	async def on_message(self, scope, message, command_found):
		return False

	async def on_reaction(self, scope, reaction):
		return False
		
	async def on_reaction_remove(self, scope, reaction):
		return False

	def add_command(self, name, cmd):
		self.cmds[name] = cmd

	async def parse_options(self, scope, parser, options):
		isValid = False

		argParseOutput = io.StringIO()
		argParseError = io.StringIO()
		try:
			with RedirectOutput(argParseOutput, argParseError):
				args = parser.parse_args(shlex.split(options))
				return args
		except:
			if argParseOutput.getvalue():
				await self.shell.print_info(scope, argParseOutput.getvalue())
			if argParseError.getvalue():
				text = argParseError.getvalue()
				if text.find(parser.prog+": error:") >= 0:
					parts = text.split(parser.prog+": error: ")
					await self.shell.print_error(scope, parts[1]+"\n"+parts[0])
				else:
					await self.shell.print_error(scope, argParseError.getvalue())
			pass

		return None

	def ensure_object_name(self, parameter, name):
		if not re.fullmatch('[a-zA-Z_][a-zA-Z0-9_]*', name):
			raise ObjectNameError(parameter, name)

	def ensure_object_id(self, parameter, name):
		if not re.fullmatch('[0-9_]+', name):
			raise ObjectIdError(parameter, name)

	def ensure_integer(self, parameter, name):
		if not re.fullmatch('[0-9_]+', name):
			raise IntegerError(parameter, name)

	def ensure_regex(self, regex):
		try:
		    re.compile(regex)
		except re.error:
		    raise RegexError(regex)

################################################################################
# MessageStream
################################################################################

class MessageStream:
	"""
	Send long messages. Automatic splitting of messages
	"""
	def __init__(self, scope):
		self.scope = scope
		self.text = ""
		self.monospace = False

	async def flush(self):
		if self.monospace:
			self.text = self.text+"\n```"
		await self.scope.channel.send(self.text)
		if self.monospace:
			self.text = "```\n"
		else:
			self.text = ""

	async def send(self, text):
		if self.monospace:
			self.monospace = False
			self.text = self.text+"\n```"

		if len(self.text)+len(text) < 1800:
			self.text = self.text+text
		else:
			await self.flush()
			self.text = self.text+text

	async def send_monospace(self, text):
		if not self.monospace:
			self.monospace = True
			self.text = self.text+"```\n"

		if len(self.text)+len(text) < 1800:
			self.text = self.text+text
		else:
			await self.flush()
			self.text = self.text+text

	async def finish(self):
		await self.flush()
