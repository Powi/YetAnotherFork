"""

Copyright (C) 2018 MonaIzquierda (mona.izquierda@gmail.com)
Copyright (C) 2020 Powi (powi@powi.fr)

This file is part of PraxisBot.

PraxisBot is free software: you can redistribute it and/or  modify
it under the terms of the GNU Affero General Public License, version 3,
as published by the Free Software Foundation.

PraxisBot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with PraxisBot.  If not, see <http://www.gnu.org/licenses/>.

"""

import shlex
import argparse
import re
import requests
import traceback
import io
import praxisbot
import nextcord
import lxml.html
import feedparser
import os
from time import mktime
import asyncio

class HTTPPlugin(praxisbot.Plugin):
	"""
	HTTP commands
	"""

	name = "HTTP"

	def __init__(self, shell):
		super().__init__(shell)

		self.cookiename_regex = re.compile('[a-zA-Z0-9_-]+')

		self.shell.create_sql_table("cookies", ["id INTEGER PRIMARY KEY", "nameid TEXT", "discord_sid INTEGER", "name TEXT", "content TEXT", "filter TEXT"])
		self.shell.create_sql_table("cookies_groups", ["id INTEGER PRIMARY KEY", "name TEXT", "discord_sid INTEGER", "nameid TEXT"])
		self.shell.create_sql_table("rss_feeds", ["id INTEGER PRIMARY KEY", "name TEXT", "discord_sid INTEGER", "discord_cid INTEGER", "url_feed TEXT", "last_update INTEGER"])

		self.add_command("if_http", self.execute_if_http)
		self.add_command("create_cookie", self.execute_create_cookie)
		self.add_command("edit_cookie_content",self.execute_edit_cookie_content)
		self.add_command("group_cookies", self.execute_group_cookies)
		self.add_command("delete_cookie", self.execute_delete_cookie)
		self.add_command("cookies", self.execute_cookies)
		self.add_command("download", self.execute_download)
		self.add_command("upload", self.execute_upload)
		self.add_command("css_selector",self.execute_css_selector)
		self.add_command("extract_attribute",self.execute_extract_attribute)
		self.add_command("extract_text",self.execute_extract_text)
		self.add_command("add_rss_feed",self.execute_add_rss_feed)
		self.add_command("rss_feeds",self.execute_rss_feeds)
		self.add_command("remove_rss_feed",self.execute_remove_rss_feed)
		
		self.feeds = {}
	
	async def on_ready(self, scope):
		rss_feeds = scope.shell.get_sql_data("rss_feeds",["id","discord_cid","name","url_feed","last_update"],{"discord_sid":scope.guild.id},True)
		for rf in rss_feeds:
			self.feeds[rf[0]] = {'name':rf[2],'url_feed':rf[3],'channel_id':rf[1],'last_update':rf[4],'discord_sid':scope.guild.id}
		#future = asyncio.run_coroutine_threadsafe(self.rss_loop(scope), asyncio.get_running_loop())
		#task = asyncio.create_task(self.rss_loop(scope))
		loop = asyncio.get_running_loop()
		while True:
			currTime = time.time()
			sleepDuration = 600 - (currTime - prevTime)
			prevTime = currTime
			if sleepDuration > 0:
				await asyncio.sleep(sleepDuration)

			await self.rss_loop(scope)
		
		
	async def rss_loop(self, scope):
		print("[RSS Loop]")
		for ID,values in self.feeds.items():
			await asyncio.sleep(1)
			if int(values['discord_sid']) != scope.guild.id:
				continue
			try:
				resp = requests.get(values['url_feed'], timeout=5.0)
			except requests.ReadTimeout:
				print("TIMEOUT")
				continue
			feed = feedparser.parse(resp.content)
			if 'bozo_exception' in feed.keys():
				print("EXCEPTION REÇUE")
				continue
			entries = feed['entries'][:4]
			entries.reverse()
			for entry in entries:
				if('published_parsed' in entry.keys()):
					parsed = entry['published_parsed']
				elif('updated_parsed' in entry.keys()):
					parsed = entry['updated_parsed']
				else:
					continue
				time = int(mktime(parsed))
				if values['last_update'] < time:
					embed = nextcord.Embed()
					embed.title = "[{}] __**{}**__".format(values['name'],entry['title'])
					embed.description = 'summary' in entry.keys() and entry['summary'][:1800] or None
					if 'link' in entry.keys():
						if embed.description:
							embed.description += "\n\n[Continue reading : <{}>]".format(entry['link'])
						else:
							embed.description = entry['link']
					chan_id = 'channel_id' in values.keys() and values['channel_id'] or None
					if chan_id:
						channel = scope.shell.find_channel(str(chan_id),scope.guild)
						if channel:
							await channel.send(embed=embed)
					values['last_update'] = time
					scope.shell.set_sql_data("rss_feeds",{'last_update':time},{'id':ID})
			await asyncio.sleep(1)
		print("END READING FEEDS")
		

	@praxisbot.command
	@praxisbot.permission_admin
	async def execute_create_cookie(self, scope, command, options, lines, **kwargs):
		"""
		Create a cookie for HTTP requests.
		"""

		scope.deletecmd = True

		parser = argparse.ArgumentParser(description=kwargs["description"], prog=command)
		parser.add_argument('id', help='ID of the cookie. This ID is used to delete the cookie.')
		parser.add_argument('name', help='Name of the cookie.')
		parser.add_argument('filter', help='Regular expression that restrict usage of the cookie.')
		parser.add_argument('--force', '-f', action='store_true', help='Replace the cookie if it already exists')
		args = await self.parse_options(scope, parser, options)
		if not args:
			return

		if len(lines) == 0:
			await self.shell.print_error(scope, "Missing cookie content. Please write the content in the same message, just the line after the command. Ex.:```\nadd_cookie my_new_cookie\nCONTENT\"```")
			return

		self.ensure_object_name("Cookie ID", args.id)

		cookieID = scope.shell.get_sql_data("cookies", ["id"], {"discord_sid": int(scope.guild.id), "nameid": str(args.id)})
		if cookieID and not args.force:
			await scope.shell.print_error(scope, "The cookie `{}` already exists.".format(args.id))
			return

		scope.shell.set_sql_data("cookies", {"name": str(args.name), "content": str("\n".join(lines)), "filter": str(args.filter)}, {"discord_sid": int(scope.guild.id), "nameid": str(args.id)})
		if cookieID:
			await scope.shell.print_success(scope, "Cookie `{}` edited.".format(args.id))
		else:
			await scope.shell.print_success(scope, "Cookie `{}` added.".format(args.id))
	
	@praxisbot.command
	@praxisbot.permission_admin
	async def execute_edit_cookie_content(self, scope, command, options, lines, **kwargs):
		"""
		Create a cookie for HTTP requests.
		"""

		scope.deletecmd = True

		parser = argparse.ArgumentParser(description=kwargs["description"], prog=command)
		parser.add_argument('id', help='ID of the cookie to edit.')
		args = await self.parse_options(scope, parser, options)
		if not args:
			return

		if len(lines) == 0:
			await self.shell.print_error(scope, "Missing cookie content. Please write the content in the same message, just the line after the command. Ex.:```\nadd_cookie my_new_cookie\nCONTENT\"```")
			return

		cookieID = scope.shell.get_sql_data("cookies", ["id"], {"discord_sid": int(scope.guild.id), "id": str(args.id)})
		if not cookieID:
			await scope.shell.print_error(scope, "The cookie `{}` doesn’t exists.".format(args.id))
			return

		scope.shell.set_sql_data("cookies", {"content": str("\n".join(lines))}, {"discord_sid": int(scope.guild.id), "id": str(args.id)})
		await scope.shell.print_success(scope, "Cookie `{}` edited.".format(args.id))
	
	@praxisbot.command
	@praxisbot.permission_admin
	async def execute_group_cookies(self, scope, command, options, lines, **kwargs):
		"""
		Create a group of cookies
		"""
		
		scope.delete_cookie = True
		
		parser = argparse.ArgumentParser(description=kwargs["description"], prog=command)
		parser.add_argument('name', help='Name of the group of cookies.')
		parser.add_argument('cookiesID', nargs="*", help='IDs of cookies to group together')
		parser.add_argument('--force', '-f', action='store_true', help='Replace the group if it already exists')
		parser.add_argument('--append', '-a', action='store_true', help='Append to the group if it already exists')
		args = await self.parse_options(scope, parser, options)
		if not args:
			return
			
		groupID = scope.shell.get_sql_data("cookies_groups", ["id"], {"discord_sid": int(scope.guild.id), "name": str(args.name)})
		if groupID and not args.force and not args.append:
			await scope.shell.print_error(scope, "The group `{}` already exists.".format(args.name))
			return
		elif args.force:
			scope.shell.delete_sql_data("cookies_groups", {"discord_sid": scope.guild.id, "name": str(args.name)})
		
		for cookie in args.cookiesID:
			scope.shell.set_sql_data("cookies_groups", {}, {"name": str(args.name), "discord_sid": scope.guild.id, "nameid": str(cookie)})
			
		if groupID:
			await scope.shell.print_success(scope, "Group `{}` edited.".format(args.name))
		else:
			await scope.shell.print_success(scope, "Group `{}` added.".format(args.name))
		

	@praxisbot.command
	async def execute_delete_cookie(self, scope, command, options, lines, **kwargs):
		"""
		Delete a cookie.
		"""

		scope.deletecmd = True

		parser = argparse.ArgumentParser(description=kwargs["description"], prog=command)
		parser.add_argument('id', help='ID of the cookie. This ID is used to delete the cookie.')
		args = await self.parse_options(scope, parser, options)
		if not args:
			return

		self.ensure_object_name("Cookie ID", args.id)

		cookieID = scope.shell.get_sql_data("cookies", ["id"], {"discord_sid": int(scope.guild.id), "nameid": str(args.id)})
		if not cookieID:
			await scope.shell.print_error(scope, "The cookie `{}` doesn't exists.".format(args.id))
			return

		scope.shell.delete_sql_data("cookies", {"discord_sid": scope.guild.id, "nameid": str(args.id)})
		await scope.shell.print_success(scope, "Cookie `{}` deleted.".format(args.id))

	@praxisbot.command
	@praxisbot.permission_admin
	async def execute_cookies(self, scope, command, options, lines, **kwargs):
		"""
		List all cookies.
		"""

		parser = argparse.ArgumentParser(description=kwargs["description"], prog=command)
		args = await self.parse_options(scope, parser, options)
		if not args:
			return

		stream = praxisbot.MessageStream(scope)
		await stream.send("**List of HTTP cookies**\n")

		cookies = scope.shell.get_sql_data("cookies",["id","nameid","filter"],{"discord_sid":scope.guild.id},True)
		if not cookies:
			await stream.finish()
			return
		for row in cookies:
			await stream.send("\n #{} {}: `{}`".format(row[0],row[1],row[2]))

		groups = scope.shell.get_sql_data("cookies_groups",["name","nameid"], {"discord_sid":scope.guild.id}, True)
		if groups:
			await stream.send("\n\n**List of cookies groups**\n")
			g_names = []
			
			# Get groups names
			for g in groups:
				g_names.append(g[0])
			g_names = list(set(g_names)) # From many iteratinos to one
			
			
			for g in g_names:
				await stream.send("\nGroup : **{}**".format(g))
				cookies = scope.shell.get_sql_data("cookies_groups",["nameid"], {"discord_sid":scope.guild.id,"name":g}, True)
				for c in cookies:
					await stream.send("\n·{}".format(c[0]))
		await stream.finish()

	@praxisbot.command
	@praxisbot.permission_script
	async def execute_if_http(self, scope, command, options, lines, **kwargs):
		"""
		Perform tests on URLs. Used with `endif`.
		"""

		parser = argparse.ArgumentParser(description=kwargs["description"], prog=command)
		parser.add_argument('url', help='First values', metavar='VALUE')
		parser.add_argument('--valid', action='store_true', help='Test if the URL is valid.')
		parser.add_argument('--not', dest='inverse', action='store_true', help='Inverse the result of the test')
		args = await self.parse_options(scope, parser, options)
		if not args:
			return


		res = False
		if args.valid:
			url = scope.format_text(args.url)
			httpResult = requests.head(url, allow_redirects=True)
			res = httpResult.ok

		if args.inverse:
			res = not res

		scope.blocks.append(praxisbot.ExecutionBlockIf(res))

	@praxisbot.command
	@praxisbot.permission_script
	async def execute_download(self, scope, command, options, lines, **kwargs):
		"""
		Download from an URL.
		"""

		parser = argparse.ArgumentParser(description=kwargs["description"], prog=command)
		parser_group = parser.add_mutually_exclusive_group()
		parser.add_argument('url', help='URL to download')
		parser.add_argument('--pdf', action="store_true", help='Send result as a pdf file (if possible)')
		parser.add_argument('--filename', help='Filename sent in Discord')
		parser.add_argument('--var', help='Variable that will contains the file')
		parser_group.add_argument('--cookie', help='Name of a cookie to send with the request.')
		parser_group.add_argument('--cookies_group', '-cg', type=str, help='Name of a goup of cookies to send with the request.')
		args = await self.parse_options(scope, parser, options)
		if not args:
			return
		
		url = scope.format_text(args.url)
		result = None
		cookies = {}
		if args.cookie:
			cookieData = scope.shell.get_sql_data("cookies", ["name", "content", "filter"], {"discord_sid": scope.guild.id, "nameid": str(args.cookie)})
			if not cookieData:
				await scope.shell.print_error(scope, "Cookie `{}` not found.".format(args.cookie))
				return
			if not re.fullmatch(cookieData[2], url):
				await scope.shell.print_error(scope, "This cookie can't be used with this URL.")
				return

			cookies[cookieData[0]] = cookieData[1]
			print("Using cookies {}".format(cookies))
		elif args.cookies_group:
			cookiesID = scope.shell.get_sql_data("cookies_groups", ["nameid"], {"discord_sid": scope.guild.id, "name": args.cookies_group},True)
			for id in cookiesID:
				nameid = id[0]
				cookieData = scope.shell.get_sql_data("cookies", ["name", "content", "filter"], {"discord_sid": scope.guild.id, "nameid": nameid})
				if not cookieData:
					await scope.shell.print_error(scope, "Cookie `{}` not found.".format(nameid))
					return
				if not re.fullmatch(cookieData[2], url):
					await scope.shell.print_error(scope, "This cookie can't be used with this URL.")
					return
				
				cookies[cookieData[0]] = cookieData[1]
			print("Using cookies {}".format(cookies))

		try:
			result = requests.get(url, allow_redirects=True, cookies=cookies, stream=True)
			if not result.ok:
				result = None

		except:
			result = None

		if not result:
			await scope.shell.print_error(scope, "The page `{}` can't be loaded.".format(url))
			return

		if args.pdf or args.filename:
			f = io.BytesIO(result.content)
			try:
				if args.filename:
					await scope.channel.send(file=nextcord.File(f,filename=args.filename))
				else:
					await scope.channel.send(file=nextcord.File(f,filename="article.pdf"))
			except nextcord.errors.HTTPException:
				await scope.shell.print_error(scope, "File impossible to send using discord (Due to 8Mo limit)")
			f.close()
		elif args.var:
			scope.vars[args.var] = result.text
		else:
			scope.vars["result"] = result.text


	@praxisbot.command
	async def execute_upload(self, scope, command, options, lines, **kwargs):
		"""
		Upload files on Discord.
		"""

		parser = argparse.ArgumentParser(description=kwargs["description"], prog=command)
		parser.add_argument('data', help='Data that will be contains in the file')
		parser.add_argument('--filename', help='Name of the file that will be uploaded')
		args = await self.parse_options(scope, parser, options)
		if not args:
			return

		f = io.BytesIO(scope.format_text(args.data).encode('UTF-8'))
		try:
			await scope.channel.send(file=nextcord.File(f,filename=args.filename or "article.pdf"))
		except nextcord.errors.HTTPException:
			await scope.shell.print_error(scope, "File impossible to send using discord (Due to 8Mo limit)")
		f.close()

	@praxisbot.command
	@praxisbot.permission_script
	async def execute_css_selector(self, scope, command, options, lines, **kwargs):
		"""
		Get a HTML element, given a css selector
		"""
	
		parser = argparse.ArgumentParser(description=kwargs["description"], prog=command)
		parser.add_argument('css_selector', help='The css selector (start with `.` for a class ; start with `#` for an id).')
		parser.add_argument('--var', help='Variable that will contains the HTML element.')
		parser.add_argument('--html_var', help='Variable with the HTML content where the HTML element is.')
		parser.add_argument('--index', help='[Defaut=0] the index to find le HTML element from the cssselect returned list')
		args = await self.parse_options(scope, parser, options)
		if not args:
			return

		if args.html_var:
			html = lxml.html.fromstring(scope.vars[args.html_var])
		elif len(lines) > 0:
			html = lxml.html.fromstring("\n".join(lines))
		else:
			html = lxml.html.fromstring(scope.vars["result"])
		
		index = args.index and int(args.index) or 0
		element = html.cssselect(args.css_selector)[index]
		if args.var:
			scope.vars[args.var] = element
		else:
			scope.vars["element"] = element
	
	@praxisbot.command
	@praxisbot.permission_script
	async def execute_extract_attribute(self, scope, command, options, lines, **kwargs):
		"""
		Get an attribute, given an HTML element
		"""
		
		parser = argparse.ArgumentParser(description=kwargs["description"], prog=command)
		parser.add_argument('attribute', help="Attribute to get from the HTML element")
		parser.add_argument('--var', help='Variable that will contains the text of the attribute.')
		parser.add_argument('--element_var', help='Variable containing the HTML element.')
		args = await self.parse_options(scope, parser, options)
		if not args:
			return
			
		element_var = args.element_var or "element"
		element = scope.vars[element_var]
		
		attribute = element.get(args.attribute)
		if args.var:
			scope.vars[args.var] = attribute
		else:
			scope.shell.print_success(scope, "The attribute value `{}`".format(attribute))
	
	@praxisbot.command
	@praxisbot.permission_script
	async def execute_extract_text(self, scope, command, options, lines, **kwargs):
		"""
		Get an attribute, given an HTML element
		"""
		
		parser = argparse.ArgumentParser(description=kwargs["description"], prog=command)
		parser.add_argument('--var', help='Variable that will contains the text of the attribute.')
		parser.add_argument('--element_var', help='Variable containing the HTML element.')
		args = await self.parse_options(scope, parser, options)
		if not args:
			return
			
		element_var = args.element_var or "element"
		element = scope.vars[element_var]
		
		text = element.text
		if args.var:
			scope.vars[args.var] = text
		else:
			scope.shell.print_success(scope, "The content is `{}`".format(text))
		
	""" V RSS FEED V """
	@praxisbot.command
	@praxisbot.permission_admin
	async def execute_add_rss_feed(self, scope, command, options, lines, **kwargs):
		"""
		Add RSS flux to follow
		"""
		
		parser = argparse.ArgumentParser(description=kwargs["description"], prog=command)
		parser.add_argument('name', help='Name given to the feed')
		parser.add_argument('url', help='URL of the RSS flux to follow')
		parser.add_argument('--channel', help='Channel where to post RSS flux')
		args = await self.parse_options(scope, parser, options)
		if not args:
			return
		
		scope.deletecmd = True
		
		test_url = feedparser.parse(args.url)
		if 'bozo_exception' in test_url.keys():
			await scope.shell.print_error(scope, "L’url `{}` ne semble pas correspondre à un flux RSS, exception : `{}` reçue.".format(args.url,test_url['bozo_exception']))
			return
		
		if args.channel:
			chan = scope.shell.find_channel(scope.format_text(args.channel).strip(), scope.guild)
		else:
			chan = scope.channel
		
		id = scope.shell.add_sql_data("rss_feeds", {"discord_sid": scope.guild.id, "discord_cid": chan.id, "url_feed": args.url, "name": args.name, "last_update": 0})
		self.feeds[id] = {'name':args.name,'url_feed':args.url,'last_update':0,'channel_id':chan.id,'discord_sid':scope.guild.id}
		
		await scope.shell.print_success(scope,"RSS Feed added")
	
	@praxisbot.command
	async def execute_rss_feeds(self, scope, command, options, lines, **kwargs):
		"""
		List all current rss feeds.
		"""

		parser = argparse.ArgumentParser(description=kwargs["description"], prog=command)
		args = await self.parse_options(scope, parser, options)
		if not args:
			return



		list_feeds = scope.shell.get_sql_data("rss_feeds",['id','name', 'discord_cid','url_feed'],{'discord_sid':scope.guild.id},True)
		if not list_feeds:
			await scope.shell.print_error(scope,"No RSS Feed")
			return
			
		stream = praxisbot.MessageStream(scope)
		await stream.send("__**List of RSS feeds**__")
		for row in list_feeds:
			chan = scope.shell.find_channel(str(row[2]), scope.guild)
			chan_name = "an unknown channel"
			if chan:
				chan_name = chan.mention

			await stream.send("\n\n **RSS Feed #{} {} in {}** – URL : <{}>".format(row[0], row[1], chan_name, row[3]))
			if len(row[1]) > 0:
				description = "```\n"+row[1]+"\n```"

		await stream.finish()
	
	@praxisbot.command
	@praxisbot.permission_admin
	async def execute_remove_rss_feed(self, scope, command, options, lines, **kwargs):
		"""
		Remove a RSS feed
		"""

		parser = argparse.ArgumentParser(description=kwargs["description"], prog=command)
		parser.add_argument('ID', help='ID of the RSS Feed to remove')
		args = await self.parse_options(scope, parser, options)
		if not args:
			return
			
		self.ensure_object_id("ID", args.ID)
		scope.deletecmd = True
		rss_feed = scope.shell.get_sql_data("rss_feeds", ["id"], {"discord_sid":scope.guild.id, "id":int(args.ID)})
		if not rss_feed:
			await scope.shell.print_error(scope, "RSS Feed #{} not found.".format(args.ID))
			return

		scope.shell.delete_sql_data("rss_feeds",{'id':args.ID})
		try:
			self.feeds.pop(rss_feed[0])
		except KeyError:
			pass	
		await scope.shell.print_success(scope, "RSS Feed removed.")
