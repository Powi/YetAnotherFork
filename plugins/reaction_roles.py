"""

Copyright (C) 2018 MonaIzquierda (mona.izquierda@gmail.com)
Copyright (C) 2020 Powi (powi@powi.fr)

This file is part of PraxisBot.

PraxisBot is free software: you can redistribute it and/or  modify
it under the terms of the GNU Affero General Public License, version 3,
as published by the Free Software Foundation.

PraxisBot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with PraxisBot.  If not, see <http://www.gnu.org/licenses/>.

"""

import shlex
import argparse
import re
import nextcord
import traceback
import datetime
import io
import sqlite3
from pytz import timezone
import praxisbot
import asyncio
from operator import xor


class RRType:
	Default=0
	Autodelete=1
	Notification=2
	OnlyOne=4

class RRPlugin(praxisbot.Plugin):
	"""
	Reaction Role commands
	"""

	name = "Reaction Role"

	def __init__(self, shell):
		super().__init__(shell)

		self.shell.create_sql_table("rr_msg", ["id INTEGER PRIMARY KEY", "discord_sid INTEGER", "discord_cid INTEGER", "discord_mid INTEGER", "title TEXT", "description TEXT", "type INTEGER", "role_conditions TEXT"])
		self.shell.create_sql_table("rr_choices", ["id INTEGER PRIMARY KEY", "rr_msg INTEGER", "emoji TEXT", "role_id INTEGER"])

		self.add_command("rrmsg", self.execute_rrmsg)
		# self.add_command("rrmsg_edit", self.execute_rrmsg_edit)
		# self.add_command("rr_add", self.execute_rr_add)
		# self.add_command("rr_rm", self.execute_rr_rm)
		# self.add_command("rr_fix", self.execute_rr_fix)

	def check_emoji(self, reaction, emoji):
		e = str(reaction.emoji)
		return e.startswith(emoji)

	async def on_reaction_remove(self, scope, reaction=None):
		await self.on_reaction(scope, reaction, remove=True)
	
	async def on_reaction(self, scope, reaction=None, remove=False):
		with scope.shell.dbcon:
			if reaction:
				print("Reaction mode")
				rrmsgs = scope.shell.get_sql_data("rr_msg",["id","discord_cid","discord_mid","type","role_conditions"],{"discord_sid":scope.guild.id,"discord_mid":reaction.message.id},True)
			else:
				print("No reaction mode")
				rrmsgs = scope.shell.get_sql_data("rr_msg",["id","discord_cid","discord_mid","type","role_conditions"],{"discord_sid":scope.guild.id},True)
			for rrm in rrmsgs:
				if reaction:
					print("There’s a reaction")
					msg = reaction.message
				else:
					print("There’s no reaction")
					chan = scope.shell.find_channel(str(rrm[1]), scope.guild)
					msg = None
					if chan:
						try:
							msg = await chan.fetch_message(int(rrm[2]))
						except:
							pass
				if msg:
					print("There’s a message")
					changes = False
					choices = {}
					reaction_already_added = []
					
					entries = scope.shell.get_sql_data("rr_choices",["id","emoji","role_id"],{"rr_msg":rrm[0]},True)
					for entry in entries:
						choices[entry[0]] = [entry[1],entry[2]]
					
					print(f"Reaction {str(reaction.emoji)}")
					choice_id = None
					for c in choices:
						print(f"Checking match between {reaction.emoji} and {choices[c][0]}")
						if self.check_emoji(reaction, choices[c][0]):
							print("match")
							choice_id = c
							break
					if not choice_id:
						print(f"Emoji {reaction.emoji} not in choices")
						return
					print(f"Reading next user: {scope.user.display_name}")
					choice_emoji = choices[choice_id][0]
					choice_role = scope.shell.find_role(str(choices[choice_id][1]), scope.guild)
					if not choice_role:
						await scope.shell.print_error(scope, f"Role `{choices[choice_id][1]}` not found.")
						return
					if not choice_id:
						print("No choice id")
						await msg.remove_reaction(reaction.emoji, scope.user)
					elif scope.user.id == scope.shell.client.user.id and not remove:
						reaction_already_added.append(choice_emoji)
					else:
						try:
							type = int(rrm[3])
							if rrm[4]:
								conditions = rrm[4].split(";")
							else:
								conditions = []
							removedRoles=[]
							print("Is there conditions?")
							if conditions:
								for c in conditions:
									if c[0] == "+":
										role = scope.shell.find_role(str(c[1:]), scope.guild)
										if not role:
											await scope.shell.print_error(scope, f"Role `{c[1]}` not found.")
											return		
										if role not in scope.user.roles:
											await scope.user.send(f"You don’t have the required role {role} to add {choice_role}")
											raise Error()
											break
									elif c[0] == "-":
										if role in scope.user.roles:
											await scope.user.send(f"You have the forbiden role {role}, you can’t add {choice_role}")
											raise Error()
											break
							print("Testing RRType")
							if type&RRType.Autodelete and remove:
								return
							if type&RRType.Autodelete:
								await msg.remove_reaction(reaction.emoji, scope.user)
							if type&RRType.OnlyOne:
								for c in choices:
									role = scope.shell.find_role(str(choices[c][1]), scope.guild)
									if not role:
										await scope.shell.print_error(scope, f"Role `{choices[c][1]}` not found.")
										return
									if role in scope.user.roles and role != choice_role:
										try:
											await msg.remove_reaction(c[0], scope.user)
										except:
											print(traceback.format_exc())
										print("Removing role")
										await scope.user.remove_roles(role)
										print("Preparing notification")
										removedRoles.append(role.name)
									print("After delete roles")
								print("No choices?")
							print("Adding/removing role")
							if choice_role in scope.user.roles and xor(type&RRType.Autodelete, remove):
								print(f"Removing role {choice_role}")
								await scope.user.remove_roles(choice_role)
								output = f"Removed role {choice_role}"
								print(f"Removed role {choice_role}")
							elif not choice_role in scope.user.roles and not remove:
								print(f"if({choice_role in scope.user.roles}) and {bool(type&RRType.Autodelete)} XOR {remove}")
								await scope.user.add_roles(choice_role)
								rRoles = ", removed roles: " + " ; ".join(removedRoles) if removedRoles else ""
								output = f"Added role {choice_role}{rRoles}"
								print(f"Adding role {choice_role}")
							else:
								print("Nothing happens")
							if type&RRType.Notification:
								await scope.user.send(output)
								
						except Error:
							pass
						except:
							print(traceback.format_exc())
							await scope.user.send(":no_entry: Your reaction role on the server \"{}\" was lost due to a technical issue.".format(scope.guild.name))

					print("I'll add not already added reactions")
					for c in choices:
						if choices[c] not in reaction_already_added:
							await msg.add_reaction(choices[c][0])

	@praxisbot.command
	@praxisbot.permission_admin
	async def execute_rrmsg(self, scope, command, options, lines, **kwargs):
		"""
		Create a Reaction Role message.
		"""

		parser = argparse.ArgumentParser(description=kwargs["description"], prog=command)
		parser.add_argument('--title', help='Title of the rr (= reaction role) message.')
		parser.add_argument('--description', help='Description of the rr message.')
		parser.add_argument('--footer', help='Footer of the rr message.')
		parser.add_argument('--channel', help='Channel where the rr message will be created.')
		parser.add_argument('--autodelete', action='store_true', help='Automatically delete reactions after giving the roles.')
		parser.add_argument('--notification', action='store_true', help='Send a notification on adding role.')
		parser.add_argument('--onlyone', action='store_true', help='Possibility to add only one role within the same message/condition.')
		parser.add_argument('--hasrole', nargs='*', help='RR conditioned to member having the given roles.')
		parser.add_argument('--nothasrole', nargs='*', help='RR conditioned to member NOT having the given roles.')
		parser.add_argument('choices', nargs='*', help='List of emoji and role. Ex: `👎 "No DM" 🤷 "Ask to DM" 👍 "DM ok".`')
		
		args = await self.parse_options(scope, parser, options)
		if not args:
			return

		if args.channel:
			chan = scope.shell.find_channel(scope.format_text(args.channel).strip(), scope.guild)
		else:
			chan = scope.channel

		if not chan:
			await scope.shell.print_error(scope, "Unknown channel.")
			return

		type = RRType.Default
		if args.onlyone:
			type += RRType.OnlyOne
		if args.notification:
			type += RRType.Notification
		if args.autodelete:
			type += RRType.Autodelete

		conditionList = []
		if args.hasrole:
			for rolename in args.hasrole:
				role = scope.shell.find_role(rolename, scope.guild)
				if not role:
					await scope.shell.print_error(scope, f"Role `{rolename}` not found.")
					return
				conditionList.append(f"+{role}")
		if args.nothasrole:
			for rolename in args.nothasrole:
				role = scope.shell.find_role(rolename, scope.guild)
				if not role:
					await scope.shell.print_error(scope, f"Role `{rolename}` not found.")
					return
				conditionList.append(f"-{role}")
		conditions = ";".join(conditionList)
			
		choices = []
		choice_emoji = None
		for c in args.choices:
			if not choice_emoji:
				choice_emoji = c
			else:
				role = scope.shell.find_role(c, scope.guild)
				if not role:
					await scope.shell.print_error(scope, f"Role `{c}` not found.")
					return
				choices.append({"emoji":choice_emoji, "role":role})
				choice_emoji = None
		
		embed = nextcord.Embed()
		description = ""
		if args.title:
			embed.title = scope.format_text(args.title)
		if args.description:
			description = scope.format_text(args.description)
		if args.footer:
			embed.set_footer(text=scope.format_text(args.footer))

		for c in choices:
			description += "\n{} {}".format(c["emoji"],c["role"])
		
		embed.description = description
		msg = await chan.send(embed=embed)
		

		for c in choices:
			try:
				await msg.add_reaction(c["emoji"])
			except:
				await scope.shell.print_error(scope, "\"{}\" is not a valid emoji.".format(c["emoji"]))
				return

		rrmsg_id = scope.shell.add_sql_data("rr_msg", {"discord_sid": int(msg.guild.id), "discord_cid": int(chan.id), "discord_mid": int(msg.id), "description": description, "title": embed.title, "type":type})
		for c in choices:
			scope.shell.add_sql_data("rr_choices", {"rr_msg": rrmsg_id, "emoji": c["emoji"], "role_id": c["role"].id})
