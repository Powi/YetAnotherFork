## Install (GNU/Linux)

`pip3 install websockets discord.py python-dateutil mkdir databases lxml cssselect feedparser`

## Run (GNU/Linux)

`python3 ./praxis.py <BOT_TOKEN>`

**This Bot is a Fork of [PraxisBot](https://github.com/MonaIzquierda/PraxisBot)**
